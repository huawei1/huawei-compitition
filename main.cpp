#include <cstdio>
#include <cstring>
#include <queue>
#include <stdlib.h>
#define MAX_EDGE 10000
#define MAX_NODE 1003
using namespace std;
const long long fd = 1;
const long long INF = fd << 100;

// 固定参数
int nodeNum, edgeNum, consumeNum;
int fcost;
int head[MAX_NODE];   // head[i]表示以node i为头的其中一条边的序号
int edgenum = 0;
int demand = 0;
int edge_array[MAX_NODE];

// 每次SPFA都要更新的参数
long long dis[MAX_NODE];
int p[MAX_NODE];
bool vis[MAX_NODE];   // in queue or not
int total_flow;

struct edge
{
    int u;
    int v;
    int cost;
    int flow;
    int cap;
    int next;  //Next edge with same head u (siblings)
}e[MAX_EDGE*4+MAX_NODE*4];

void add(int u,int v,int cap,int cost)
{
    e[edgenum].u = u;
    e[edgenum].v = v;
    e[edgenum].cap = cap;
    e[edgenum].cost = cost;
    e[edgenum].flow = 0;
    e[edgenum].next = head[u];
    head[u] = edgenum++;
    e[edgenum].u = v;
    e[edgenum].v = u;
    e[edgenum].cap = 0;
    e[edgenum].cost = -cost;
    e[edgenum].flow = 0;
    e[edgenum].next = head[v];
    head[v] = edgenum++;
}

// node s(1000)开始, node t(1002)结束, return 能产生的最大的流的成本
long SPFA()
{
    queue <int> q;
    long total_transmission_cost=0;
    total_transmission_cost = total_flow = 0;
    while(1)
    {

        dis[1000] = 0;
        for(int i = 0; i <= nodeNum; i++)
            dis[i] =  INF;
        dis[1001] = INF;
        dis[1002] = INF;
        memset(vis,false,sizeof(vis));
        memset(p,-1,sizeof(p));
        q.push(1000);
        while(!q.empty())
        {
            int u = q.front();
            q.pop();
            vis[u] = false;
            for(int k = head[u]; k != -1; k = e[k].next)
            {
                int v = e[k].v;
                if(e[k].cap > e[k].flow && dis[v] > dis[u] + e[k].cost)
                {
                    dis[v] = dis[u] + e[k].cost;
                    p[v] = k;
                    if(!vis[v])
                    {
                        vis[v] = true;
                        q.push(v);
                    }
                }
            }
        }
        if(dis[1002] == INF)
            break;
        int AP_flow = 99999999;
        for(int path = p[1002]; path != -1; path = p[e[path].u])   // Backtracking the augmented path
            AP_flow = min(AP_flow,e[path].cap - e[path].flow);  // Select the minimum available cap as AP_flow
        for(int path = p[1002]; path != -1; path = p[e[path].u])   // Backtracking the augmented path again
        {
            e[path].flow += AP_flow;
            e[path^1].flow -= AP_flow;
        }
        total_transmission_cost += dis[1002] * AP_flow;
        total_flow += AP_flow;
    }
    return total_transmission_cost;
}

// Return total cost if successful, otherwise -1
long adapt_function(const int* POP){
    long total_cost = 0;

    //初始化: 清空flow
    for(int i=0; i<edgenum; i++){
        e[i].flow = 0;
    }

    //初始化: 过一遍解 设置cap, 累计部署成本
    for(int i=0; i< nodeNum; i++){
        if(POP[i]==1){
            // 厂址与t相连的边的cap设为INF
            e[edge_array[i]].cap=INF;
            // 累计部署成本
            total_cost += fcost;
        } else
            e[edge_array[i]].cap=0;
    }

    total_cost += SPFA();

    // Fails
    if(total_flow != demand)
        total_cost = -1;

    return total_cost;
}

void deploy_server(char * topo[MAX_EDGE_NUM], int line_num, char * filename)
{
    int i = 0, counter = 4;
    int u, v, cap, cost, j = 0;
    double totalCost = 0, totalNeed = 0;
    // netnode * matrix[1000][1000];
    const char *split = " ";
    char *p;

    //读取前三个数字:nodeNum, edgeNum, consumeNum
    p = strtok(topo[0],split);
    while(p!=NULL){
        i++;
        if(i == 1)
            nodeNum = atoi(p);
        else if (i == 2)
            edgeNum = atoi(p);
        else
            consumeNum = atoi(p);
        p = strtok(NULL, split);
    }

    //读取部署成本:fcost
    fcost = atoi(topo[2]);

    //读取侯选厂址图：初始化
    edgenum = 0;                      // 计算用图中的边的序号:[0, 4*edgeNum]
    memset(head,-1,sizeof(head));     // head[i]表示以node i为头的其中一条边的序号
    //读取侯选厂址图
    for(i = 0;i<edgeNum;i++){
        p = strtok(topo[counter], split);
        j=0;
        //可加速
        while(p!=NULL){
            j++;
            if(j == 1)
                u = atoi(p);
            else if(j == 2)
                v = atoi(p);
            else if(j == 3)
                cap = atoi(p);
            else{
                cost = atoi(p);
                totalCost+=cost;
            }
            p = strtok(NULL, split);
        }
        //一共建立4条边
        add(u,v,cap,cost);
        add(v,u,cap,cost);

        counter++;
    }

    //构建第二source s*(1001)
    int demand_of_one_consumer;
    int consumer, linked_candidate;

    for(i=0; i<consumeNum; i++){
        // 开始读取
        p = strtok(topo[++counter], split);
        j=0;
        //可加速
        while(p!=NULL){
            j++;
            if(j == 1)
                consumer = atoi(p);
            else if(j == 2)
                linked_candidate = atoi(p);
            else if(j == 3)
                demand_of_one_consumer = atoi(p);
            p = strtok(NULL, split);
        }

        // 构图
        add(1001, linked_candidate, demand_of_one_consumer, 0);
        demand += demand_of_one_consumer;
    }

    // 构建第一source s(1000)
    add(1000,1001,demand,0);

    //每一个候选厂址与t(1002)拉一条边
    for(i = 0; i < nodeNum; i++){
        // 记录: 所有连接候选厂址与t的边 的序号edgenum
        edge_array[i] = edgenum;
        add(i,1002,0,0);
    }

    // 后面就不知道啦,或许可以生成一些简单的POP,再调用适应度函数来调试一下，但是我不会测试。。。
}
