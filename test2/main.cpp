#include <cstdio>
#include <cstring>
#include <queue>
using namespace std;
const int MAX = 110;
const long long fd = 1;
const long long INF = fd << 60;
int n,m;

struct edge
{
    int u;
    int v;
    long long cost;
    int flow;
    int cap;
    int next;  //Next edge with same head u (siblings)
}e[50000];

//struct node
//{
//    int u;
//    int v;
//    int w;
//}E[5050];

long long dis[MAX];
int first[MAX];
int p[MAX];
bool vis[MAX];   //in queue or not
int edgenum;
int f,d,cap;
long long c;

void add(int u,int v,long long w,int num)
{
    e[edgenum].u = u;
    e[edgenum].v = v;
    e[edgenum].cap = num;
    e[edgenum].cost = w;
    e[edgenum].flow = 0;
    e[edgenum].next = first[u];
    first[u] = edgenum++;
    e[edgenum].u = v;
    e[edgenum].v = u;
    e[edgenum].cap = 0;
    e[edgenum].cost = -w;
    e[edgenum].flow = 0;
    e[edgenum].next = first[v];
    first[v] = edgenum++;
}

void EK()
{
    queue <int> q;
    c = f = 0;
    while(1)
    {
        dis[0] = 0;
        for(int i = 1;i <= n; i++)
            dis[i] =  INF;
        memset(vis,false,sizeof(vis));
        memset(p,-1,sizeof(p));
        q.push(0);
        while(!q.empty())
        {
            int u = q.front();
            q.pop();
            vis[u] = false;
            for(int k = first[u]; k != -1; k = e[k].next)
            {
                int v = e[k].v;
                if(e[k].cap > e[k].flow && dis[v] > dis[u] + e[k].cost)
                {
                    dis[v] = dis[u] + e[k].cost;
                    p[v] = k;
                    if(!vis[v])
                    {
                        vis[v] = true;
                        q.push(v);
                    }
                }
            }
        }
        if(dis[n] == INF)
            break;
        int a = 999999999;
        for(int path = p[n]; path != -1; path = p[e[path].u])//这里的u是指第u条边，不再是邻接矩阵里面的顶点
            a = min(a,e[path].cap - e[path].flow);
        for(int u = p[n]; u != -1; u = p[e[u].u])//增广,这里的u是指第u条边，不再是邻接矩阵里面的顶点
        {
            e[u].flow += a;
            e[u^1].flow -= a;//注意看这里的亦或符号 比如u为2，亦或后就是3了，恰好第二条边的反向边就是第三条边
        }
        c += dis[n] * a;//注意需要乘以距离(路径单位费用和)
        f += a;
    }
}

int main()
{
    int i,j;
    while(scanf("%d %d",&n,&m)!=EOF)
    {
        edgenum = 0;
        memset(first,-1,sizeof(first));
        for(i = 0;i < m; i++)
        {
            scanf("%d %d %d %lld",&E[i].u,&E[i].v,&E[i].cap,&E[i].w);
        }
        scanf("%d",&d);
        add(0,1,0,d);
        for(i = 0; i < m; i++)
        {
            add(E[i].u,E[i].v,E[i].w,E[i].cap);
            add(E[i].v,E[i].u,E[i].w,E[i].cap);
        }
        EK();
        if(f == d)
            printf("%lld\n",c);
        else
            printf("Impossible.\n");
    }
    return 0;
}