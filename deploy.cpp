#include "deploy.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

//function entrance
void deploy_server(char * topo[MAX_EDGE_NUM], int line_num,char * filename)
{
    int nodeNum, edgeNum, consumeNum, fcost, i = 0, counter = 4;
	int start, end, band, cost, j = 0;
	double totalCost = 0, totalNeed = 0;
	netnode * matrix[1000][1000];
    const char *split = " ";
    char *p;
    p = strtok(topo[0],split);
    while(p!=NULL){
        i++;
        if(i == 1)
            nodeNum = atoi(p);
        else if (i == 2)
            edgeNum = atoi(p);
        else
            consumeNum = atoi(p);
        p = strtok(NULL, split);
    }
	fcost = atoi(topo[2]);
	for(i = 0;i<nodeNum;i++){
		p = strtok(topo[counter], split);
		while(p!=NULL){
			j++;
			if(j == 1)
				start = atoi(p);
			else if(i == 2)
				end = atoi(p);
			else if(i == 3)
				band = atoi(p);
			else{
				cost = atoi(p);
				totalCost+=cost;
			}
			p = strtok(NULL, split);
		}
		matrix[start][end]->band = band;
		matrix[start][end]->cost = cost;
		matrix[end][start]->band = band;
		matrix[end][start]->cost = cost;
		counter++;
	}
	for(i = 0;i<consumeNum;i++){
		p = strtok(topo[counter], split);
	}

	double mu, cigma1, cigma2, pc0, pc1, pc2, pm0, pm1, pm2,alpha, beta;
	int u, t;
	double v, randNum, adapt[N], adapt2[N];
	//the population array, each row is an individual, the column number is the number of potential facilities.
	unsigned int *pop[N], *spop[N];
    unsigned int *temp;
    unsigned int temp2;
    unsigned int *tamplate = (unsigned int*)malloc(sizeof(unsigned int)*nodeNum);
	for(u = 0;u<N;u++){
		pop[u] = (unsigned int*)malloc(sizeof(unsigned int)*nodeNum);
		spop[u] = (unsigned int*)malloc(sizeof(unsigned int)*nodeNum);
	}
	srand(time(NULL));
	v = (double)fcost/(totalCost*totalNeed/(consumeNum*consumeNum));
	//generate initial population
	for(u = 0;u<N;u++){
		for(i = 0;i<nodeNum;i++){
			randNum = (double)(rand()%100)/99.0;
			if(v>=mu){
				if(randNum<=cigma1)
					pop[u][i] = 1;
				else
					pop[u][i] = 0;
			}
			else{
				if(randNum<=cigma2)
					pop[u][i] = 1;
				else
					pop[u][i] = 0;
			}
		}
	}
	//calculate adaptation function for each individual
    for(t = 0;t<TMAX;t++){
        for(u = 0;u<N;u++){
            adapt[u] = 
        }
        //select operation
        int select1, select2;
        int maxAdapt = -1,minAdapt = 1000000;
        double avgAdapt = 0; 
        for(i = 0;i<N;i++){
            select1 = rand()%N;
            select2 = rand()%N;
            if(adapt[select1]>adapt[select2]){
                spop[i] = pop[select1];
                adapt2[i] = adapt[select1];
                if(adapt[select1]>maxAdapt)
                    maxAdapt = adapt[select1];
                if(adapt[select1]<minAdapt)
                    minAdapt = adapt[select1];
                avgAdapt +=adapt[select1];
            }
            else{
                spop[i] = pop[select2];
                adapt2[i] = adapt[select2];
                if(adapt[select2]>maxAdapt)
                    maxAdapt = adapt[select2];
                if(adapt[select2]<minAdapt)
                    minAdapt = adapt[select2];
                avgAdapt +=adapt[select2];
            }
        }
        avgAdapt/=N;
        //cross operation
        double pc;
        if((avgAdapt/maxAdapt>alpha)&&((double)minAdapt/maxAdapt>=pc1/pc2))
            pc = pc0;
        else if((avgAdapt/maxAdapt>alpha)&&((double)minAdapt/maxAdapt<pc1/pc2)&&((double)minAdapt/maxAdapt>beta)){
            pc = pc2-(pc2-pc1)/(1-(double)minAdapt/maxAdapt);
        }
        else
            pc = pc2;
        //do the random permutation
        for(i = 0;i<=N-2;i++){
            randNum = rand()%(N-i)+i;
            temp = spop[i];
            spop[i] = spop[randNum];
            spop[randNum] = temp;
        }
        for(i = 0;i<=N/2-1;i++){
            randNum = (double)(rand()%100)/99.0;
            if(randNum<pc){
                //generate template for crossing
                for(j = 0;j<nodeNum;j++){
                    if(j<3*nodeNum/10)
                        tamplate[j] = 1;
                    else
                        tamplate[j] = 0;
                }
                for(j = 0;j<=nodeNum-2;j++){
                    randNum = rand()%(N-j)+j;
                    temp2 = tamplate[j];
                    tamplate[j] = tamplate[randNum];
                    tamplate[randNum] = temp2;
                }
                for(j = 0;j<nodeNum;j++){
                    if(tamplate[j] == 1){
                        temp2 = spop[2*i][j];
                        spop[2*i][j] = spop[2*i+1][j];
                        spop[2*i+1][j] = temp2;
                    }
                }
            }
        }
        for(u = 0;u<N;u++){
            adapt[u] = 
        }
        maxAdapt = -1;
        minAdapt = 1000000;
        avgAdapt = 0;
        for(u = 0;u<N;u++){
            if(adapt[u]>maxAdapt)
                maxAdapt = adapt[u];
            if(adapt[u]<minAdapt)
                minAdapt = adapt[u];
            avgAdapt+=adapt[u];
        }
        avgAdapt/=N;
        //calculate pm
        double pm;
        if((avgAdapt/maxAdapt>alpha)&&((double)minAdapt/maxAdapt>=pm1/pm2))
            pm = pm2;
        else if((avgAdapt/maxAdapt>alpha)&&((double)minAdapt/maxAdapt>beta)&&((double)minAdapt/maxAdapt<pm1/pm2))
            pm = pm2-(pm2-pm1)/(1-(double)minAdapt/maxAdapt);
        else
            pm = pm0;
        //mutation
        for(u = 0;u<N;u++){
            randNum = (double)(rand()%100)/99.0;
            if(randNum<pm){
                randNum = rand()%nodeNum;
                spop[u][randNum] = 1- spop[u][randNum];
            }
        }        
    }

	//printf("%d %d %d\n", nodeNum, edgeNum, consumeNum);
	// output content
	char * topo_file = (char *)"17\n\n0 8 0 20\n21 8 0 20\n9 11 1 13\n21 22 2 20\n23 22 2 8\n1 3 3 11\n24 3 3 17\n27 3 3 26\n24 3 3 10\n18 17 4 11\n1 19 5 26\n1 16 6 15\n15 13 7 13\n4 5 8 18\n2 25 9 15\n0 7 10 10\n23 24 11 23";

	// 直接调用输出文件的方法输出到指定文件中(ps请注意格式的正确性，如果有解，第一行只有一个数据；第二行为空；第三行开始才是具体的数据，数据之间用一个空格分隔开)
	write_result(topo_file, filename);

}
